# Plugins included in this collection


## Action

### ntih.random.pipe

This plugin allows piping output from a local command to a remote one.
It has only been tested on ssh!

**Dependencies**

`ntih.random.ssh`

In order to be able to stream binary data over the ssh connection,
 a custom ssh connection class has to be used.
`ansible.builtin.ssh` doesn't support that and [integration was refused][ansible ssh PR].
See the example below.

**Example usage**

```yaml
- hosts: all
  connection: ntih.random.ssh
  tasks:
    - name: Transfer docker image
      ntih.random.pipe:
        local: docker save image:tag
        remote: docker load
```

[ansible ssh PR]: https://github.com/ansible/ansible/pull/74119
