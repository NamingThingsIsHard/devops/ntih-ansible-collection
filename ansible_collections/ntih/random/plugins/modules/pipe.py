#!/usr/bin/python
"""
ntih-ansible-collection
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from __future__ import absolute_import, division, print_function

__metaclass__ = type

DOCUMENTATION = r'''
---
module: pipe
version_added: "1.0.0"
short_description: Pipe local command to a remote command
description:
    - The C(pipe) action executes a command locally and pipes the output thereof to a remote host.
    - |
      The same can also be achieved with text output using
      ::
        - shell: remote_command
          input: "{{ lookup('pipe', 'local_command') }}"

options:
  local:
    description:
    - The command to execute on the local machine
    type: str
    required: yes
  remote:
    description:
    - The command to execute on the remote machine.
    - Its input will be the stdoutput of the local command
    type: str
    required: yes
seealso:
- module: ansible.builtin.command
- module: ansible.builtin.script
- module: ansible.builtin.shell
- module: ansible.builtin.pipe
author:
- LoveIsGrief (@LoveIsGrief)
'''

EXAMPLES = r'''
- name: Copy file with owner and permissions
  ansible.builtin.pipe:
    local: docker save {{ image }}
    remote: docker load
'''

RETURN = r'''
local.rc:
    description: Return code of the local command
    returned: always
    type: int
    sample: 0
remote.rc:
    description: Return code of the remote command
    returned: always
    type: int
    sample: 0
'''
