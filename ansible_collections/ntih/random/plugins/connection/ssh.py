"""
ntih-ansible-collection
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

import io
import time

from ansible.errors import AnsibleConnectionFailure
from ansible.module_utils.common.text.converters import to_native
from ansible.plugins.connection.ssh import (
    Connection as SshConnection,
    DOCUMENTATION as SSH_DOCUMENTATION,
)
from ansible.utils.display import Display

DOCUMENTATION = SSH_DOCUMENTATION

display = Display()


class Connection(SshConnection):
    def _send_initial_data(self, fh, in_data, ssh_process):
        '''
        Writes initial data to the stdin filehandle of the subprocess and closes
        it. (The handle must be closed; otherwise, for example, "sftp -b -" will
        just hang forever waiting for more commands.)
        '''
        if isinstance(in_data, io.IOBase):
            return self._send_initial_file_data(fh, in_data, ssh_process)
        else:
            super()._send_initial_data(fh, in_data, ssh_process)

    def _send_initial_file_data(self, fh, in_fh, ssh_process):
        '''
        Writes initial file data to the stdin file-handle of the subprocess and closes
        it. (The handle must be closed; otherwise, for example, "sftp -b -" will
        just hang forever waiting for more commands.)

        :type fh: io.RawIOBase
        :type in_fh: io.IOBase
        '''

        display.debug(u'Sending initial file data')
        bytes_written = 0
        try:
            while True:
                bytes_read = in_fh.read(io.DEFAULT_BUFFER_SIZE)
                if not (bytes_read and len(bytes_read) > 0):
                    # Nothing more to write
                    break

                fh.write(bytes_read)
                bytes_written += len(bytes_read)

                if not in_fh.readable():
                    break
            fh.close()
        except (OSError, IOError) as e:
            # The ssh connection may have already terminated at this point, with a more useful error
            # Only raise AnsibleConnectionFailure if the ssh process is still alive
            time.sleep(0.001)
            ssh_process.poll()
            if getattr(ssh_process, 'returncode', None) is None:
                raise AnsibleConnectionFailure(
                    'Data could not be sent to remote host "%s". '
                    'Make sure this host can be reached over ssh: %s' % (self.host, to_native(e)),
                    orig_exc=e
                ) from e

        display.debug(u'Sent initial file data (%d bytes)' % bytes_written)
