# NTIH Ansible Collection

A collection of [plugins] and modules for ansible

## Installation

From ansible galaxy
```bash
ansible-galaxy collection install ntih.random
```

From source:
```bash
ansible-galaxy collection install 'git+https://gitlab.com/NamingThingsIsHard/devops/ntih-ansible-collection.git#ansible_collections/ntih/random'
```

## Testing

```bash
vagrant up
vagrant ssh-config > ssh_config
ansible-playbook -i inventory.yaml --ssh-common-args="-F ssh_config" playbook.yml
```

[plugins]: plugins/README.md
